<?php
/*
  ./app/vues/templates/defaut.php
 */
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include '../app/vues/templates/partials/_head.php'; ?>
  </head>

  <body>
    <?php include '../app/vues/templates/partials/_nav.php'; ?>

    <!-- CONTENU -->
    <main>
        <?php echo $zoneContent1; ?>
        <hr>
    </main>

    <!-- Footer -->
    <?php include '../app/vues/templates/partials/_footer.php'; ?>

    <!-- Scripts -->
    <?php include '../app/vues/templates/partials/_scripts.php'; ?>

  </body>

</html>
